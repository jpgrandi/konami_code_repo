﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Combo {
	public string[] buttons;
	private int currIndex = 0;
	public float TimeBetweenPresses = 0.8f;
	private float LastPress;

	public Combo(string[] b) {
		buttons = b;
	}
		
	public bool Check() {
		if (Time.time > LastPress + TimeBetweenPresses) currIndex = 0; {
			if (currIndex < buttons.Length) {
				if ((buttons[currIndex] == "down" && Input.GetKeyDown(KeyCode.DownArrow)) ||
					(buttons[currIndex] == "up" && Input.GetKeyDown(KeyCode.UpArrow)) ||
					(buttons[currIndex] == "left" && Input.GetKeyDown(KeyCode.LeftArrow)) ||
					(buttons[currIndex] == "right" && Input.GetKeyDown(KeyCode.RightArrow)) ||
					(buttons[currIndex] == "a" && Input.GetKeyDown(KeyCode.A)) ||
					(buttons[currIndex] == "b" && Input.GetKeyDown(KeyCode.B))) {
					LastPress = Time.time;
					currIndex++;
				}

				if (currIndex >= buttons.Length){
					currIndex = 0;
					return true;
				}
				else return false;
			}
		}
		return false;
	}
}
	
public class comboManager : MonoBehaviour {
	public Combo konamiCode;
	public Combo newKonamiCode;
	public Combo key1, key2, key3, key4, key5, key6;
	public string button1, button2, button3, button4, button5, button6;
	public GameObject underscorePrefab;
	public Transform spot01, spot02, spot03, spot04, spot05, spot06;

	void Start () {
		List<string> codeInputs = new List<string> ();
		codeInputs.Add ("up");
		codeInputs.Add ("down");
		codeInputs.Add ("left");
		codeInputs.Add ("right");
		codeInputs.Add ("b");
		codeInputs.Add ("a");
		string button1 = (string)codeInputs [Random.Range(0, codeInputs.Count)];
		string button2 = (string)codeInputs [Random.Range(0, codeInputs.Count)];
		string button3 = (string)codeInputs [Random.Range(0, codeInputs.Count)];
		string button4 = (string)codeInputs [Random.Range(0, codeInputs.Count)];
		string button5 = (string)codeInputs [Random.Range(0, codeInputs.Count)];
		string button6 = (string)codeInputs [Random.Range(0, codeInputs.Count)];
		Debug.Log (button1 + " " + button2 + " " + button3 + " " + button4 + " " + button5 + " " + button6);
		konamiCode = new Combo(new string[] {"up", "up", "down", "down", "left", "right", "b", "a"});
		newKonamiCode = new Combo(new string[] {button1, button2, button3, button4, button5, button6});
		key1 = new Combo (new string[] { button1 });
		key2 = new Combo (new string[] { button2 });
		key3 = new Combo (new string[] { button3 });
		key4 = new Combo (new string[] { button4 });
		key5 = new Combo (new string[] { button5 });
		key6 = new Combo (new string[] { button6 });
	}
	
	// Update is called once per frame
	void Update () {
		if (konamiCode.Check())
		{
			Debug.Log("konamicode");
		}
		if (newKonamiCode.Check ()) {
			SceneManager.LoadScene (3);
		}
		if (key1.Check()) {
			var underscore1 = (GameObject)Instantiate (
				underscorePrefab,
				spot01.position,
				spot01.rotation);
			Destroy (underscore1, 1.0f);
		}
		if (key2.Check()) {
			var underscore2 = (GameObject)Instantiate (
				underscorePrefab,
				spot02.position,
				spot02.rotation);
			Destroy (underscore2, 1.0f);
		}		if (transform.position.y <= -290) {
			transform.position = new Vector3 (66, 112, 0);
		}
		if (key3.Check()) {
			var underscore3 = (GameObject)Instantiate (
				underscorePrefab,
				spot03.position,
				spot03.rotation);
			Destroy (underscore3, 1.0f);
		}
		if (key4.Check()) {
			var underscore4 = (GameObject)Instantiate (
				underscorePrefab,
				spot04.position,
				spot04.rotation);
			Destroy (underscore4, 1.0f);
		}
		if (key5.Check()) {
			var underscore5 = (GameObject)Instantiate (
				underscorePrefab,
				spot05.position,
				spot05.rotation);
			Destroy (underscore5, 1.0f);
		}
		if (key6.Check ()) {
			var underscore6 = (GameObject)Instantiate (
				underscorePrefab,
				spot06.position,
				spot06.rotation);
			Destroy (underscore6, 1.0f);
		}
	}
}
