﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class countdown : MonoBehaviour {
	public float timeLeft = 60.0f;
	public Text scoreText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		timeLeft -= Time.deltaTime;
		scoreText.text = timeLeft.ToString ();

		if (timeLeft <= 0) {
			SceneManager.LoadScene (2);
		}
	}
}
