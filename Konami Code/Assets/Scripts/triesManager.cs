﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class triesManager : MonoBehaviour {
	public int triesLeft;
	public Text triesText;

	// Use this for initialization
	void Start () {
		triesLeft = 30;

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.A) ||
			Input.GetKeyDown(KeyCode.B) ||
			Input.GetKeyDown(KeyCode.UpArrow) ||
			Input.GetKeyDown(KeyCode.DownArrow) ||
			Input.GetKeyDown(KeyCode.LeftArrow) ||
			Input.GetKeyDown(KeyCode.RightArrow)) {
			triesLeft--;
		}
		triesText.text = triesLeft.ToString ();
		if (triesLeft <= 0) {
			SceneManager.LoadScene (2);
		}
	}
}
