﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class menuButtons : MonoBehaviour {

	// Use this for initialization
	public void startGame() {
		SceneManager.LoadScene (1);
	}

	public void exitGame() {
		Application.Quit();
		UnityEditor.EditorApplication.isPlaying = false;
	}

	public void backToMenu() {
		SceneManager.LoadScene (0);
	}
}
