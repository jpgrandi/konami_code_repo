﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingTitle : MonoBehaviour { 
	int jitter;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		jitter = Random.Range (-30, 30);
		transform.Translate (1 * jitter / 10, 0, 0);
	}
}